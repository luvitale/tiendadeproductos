@extends('layouts.master')

@section('title', 'Listado de productos')

@section('card-header')
    Listado de productos
    <a href="{{ route('products.create') }}" class="btn btn-success btn-sm float-right">Nuevo producto</a>
@stop

@section('card-body')
    @if(session('info'))
        <div class="alert alert-success">
            {{ session('info') }}
        </div>
    @endif
    <table class="table table-hover table-sm">
        <thead>
            <tr>
                <th>Descripción</th>
                <th>Precio</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
            @foreach($products as $product)
            <tr>
                <td>
                    {{ $product->description }}
                </td>
                <td>
                    {{ $product->price }}
                </td>
                <td>
					<a href="{{ route('products.edit', $product->id) }}" class="btn btn-warning btn-sm">Editar</a>
                    <a href="javascript: document.getElementById('delete-{{ $product->id }}').submit()" class="btn btn-danger btn-sm">Eliminar</a>
                    <form id="delete-{{ $product->id }}" action="{{ route('products.destroy', $product->id) }}" method="post">
                        @method('delete')
                        @csrf
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@stop

@section('footer')
	<div class="card-footer">
		Hola {{ auth()->user()->name }}
		<a href="javascript: document.getElementById('logout').submit()" class="btn btn-danger btn-sm float-right">Cerrar sesión</a>
		<form action="{{ route('logout') }}" id="logout" style="display:none" method="post">
			@csrf
		</form>
	</div>
@stop