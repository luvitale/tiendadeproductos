@extends('layouts.master')

@section('title', 'Crear producto')

@section('card-header')
    Crear producto
@stop

@section('card-body')
    <form action="{{ route('products.store') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="">Descripción</label>
            <input type="text" class="form-control" name="description">
        </div>
        <div class="form-group">
            <label for="">Precio</label>
            <input type="number" class="form-control" name="price" min="0">
        </div>
        <button type="submit" class="btn btn-primary">Guardar</button>
        <a href="{{ route('products.index') }}" class="btn btn-danger">Cancelar</a>
    </form>
@stop