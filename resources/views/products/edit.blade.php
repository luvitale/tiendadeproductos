@extends('layouts.master')

@section('title', 'Editar producto')

@section('card-header')
    Editar producto
@stop

@section('card-body')
    <form action="{{ route('products.update', $product->id) }}" method="post">
		@method('put')
        @csrf
        <div class="form-group">
            <label for="">Descripción</label>
            <input type="text" value="{{ $product->description }}" class="form-control" name="description">
        </div>
        <div class="form-group">
            <label for="">Precio</label>
            <input type="number" value="{{ $product->price }}" class="form-control" name="price" min="0">
        </div>
        <button type="submit" class="btn btn-primary">Guardar</button>
        <a href="{{ route('products.index') }}" class="btn btn-danger">Cancelar</a>
    </form>
@stop